import { NextFunction, Request, RequestHandler, Response } from 'express';
import 'reflect-metadata';
import { MetaKeys } from './MetaKeys';

export const use = (middleware: RequestHandler) => {
    return (target: any, key: string, desc: PropertyDescriptor) => {
        const middlewares =
            Reflect.getMetadata(MetaKeys.middleware, target, key) || [];

        middlewares.push(middleware);

        Reflect.defineMetadata(MetaKeys.middleware, middlewares, target, key);
    };
};

export const validateBody = (...keys: string[]) => {
    return (target: any, key: string, desc: PropertyDescriptor) => {
        Reflect.defineMetadata(MetaKeys.validator, keys, target, key);
    };
};
