export enum MetaKeys {
    path = 'path',
    method = 'method',
    middleware = 'middleware',
    validator = 'validator',
}
