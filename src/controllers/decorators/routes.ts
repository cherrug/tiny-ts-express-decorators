import { RequestHandler } from 'express';
import 'reflect-metadata';
import { Methods } from '../../AppRouter';
import { MetaKeys } from './MetaKeys';

interface IRouteHandlerDescriptor extends PropertyDescriptor {
    value?: RequestHandler;
}

const routeBinder = (method: string) => {
    return (path: string) => {
        return (target: any, key: string, desc: IRouteHandlerDescriptor) => {
            Reflect.defineMetadata(MetaKeys.method, method, target, key);
            Reflect.defineMetadata(MetaKeys.path, path, target, key);
        };
    };
};

export const get = routeBinder(Methods.get);
export const post = routeBinder(Methods.post);
export const del = routeBinder(Methods.del);
export const put = routeBinder(Methods.put);
