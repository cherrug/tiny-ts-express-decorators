import 'reflect-metadata';
import { AppRouter, Methods } from '../../AppRouter';
import { bodyValidator } from '../middleware';
import { MetaKeys } from './MetaKeys';

const controller = (rootPath: string) => {
    return (target: Function) => {
        const router = AppRouter.getInstance();
        const { prototype } = target;

        for (const key of Object.keys(prototype)) {
            const routeHandler = target.prototype[key];
            const method: Methods = Reflect.getMetadata(
                MetaKeys.method,
                prototype,
                key,
            );
            const path =
                rootPath + Reflect.getMetadata(MetaKeys.path, prototype, key);

            const middlewares =
                Reflect.getMetadata(MetaKeys.middleware, prototype, key) || [];

            const bodyProps =
                Reflect.getMetadata(MetaKeys.validator, prototype, key) || [];

            if (path) {
                router[method](
                    path,
                    ...middlewares,
                    bodyValidator(bodyProps),
                    routeHandler,
                );
            }

            // implementation goes here
        }
    };
};

export { controller };
