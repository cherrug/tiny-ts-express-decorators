import { NextFunction, Request, RequestHandler, Response } from 'express';

export const requireAuth = (
    req: Request,
    res: Response,
    next: NextFunction,
): void => {
    if (req.session && req.session.loggedIn) {
        next();
        return;
    }

    res.send('You are not permitted');
};

export const bodyValidator = (keys: string[]): RequestHandler => {
    return (req: Request, res: Response, next: NextFunction) => {
        if (!req.body) {
            res.status(422).send('Invalid request. Body missing');
            return;
        }

        for (const key of keys) {
            if (!req.body[key]) {
                res.status(422).send(`Missing property '${key}'`);
                return;
            }
        }

        next();
    };
};
