import { NextFunction, Request, Response } from 'express';
import { controller, get, post, validateBody } from './decorators';

@controller('/auth')
export default class AuthController {
    @get('/login')
    public getLogin(req: Request, res: Response): void {
        res.send(`
        <form method="POST">
            <div>
                <label>Email</label>
                <input name="email" />
            </div>
            <div>
                <label>Password</label>
                <input name="password" type="password" />
            </div>
            <button type="submit">Submit</button>
        </form>
        `);
    }

    @post('/login')
    @validateBody('email', 'password')
    public postLogin(req: Request, res: Response) {
        const { email, password } = req.body;

        if (email === 'test' && password === 'test') {
            req.session = { loggedIn: true };
        }
        res.redirect('/');
    }

    @get('/logout')
    public logout(req: Request, res: Response) {
        req.session = undefined;
        res.redirect('/');
    }
}
