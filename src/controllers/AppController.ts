import { Request, Response } from 'express';
import { controller, get, use } from './decorators';
import { requireAuth } from './middleware';

@controller('')
export class AppController {
    @get('/')
    public home(req: Request, res: Response) {
        if (req.session && req.session.loggedIn) {
            res.send(`
                <div>
                    <h3>You are logged in</h3>
                    <a href="/auth/logout">Logout</a>
                </div>
            `);
        } else {
            res.send(`
                <div>
                    <h3>You are not logged in</h3>
                    <a href="/auth/login">Login</a>
                </div>
            `);
        }
    }

    @get('/protected')
    @use(requireAuth)
    public protected(req: Request, res: Response) {
        res.send('In protected area');
    }
}
