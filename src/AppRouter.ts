import { Router } from 'express';

export enum Methods {
    get = 'get',
    post = 'post',
    del = 'delete',
    put = 'put',
}

export class AppRouter {
    public static getInstance(): Router {
        if (!AppRouter.instance) {
            AppRouter.instance = Router();
        }

        return AppRouter.instance;
    }
    private static instance: Router;
}
