# Very simplified ts-express-decorators like

This small project was done in order to understand how `decorators` work in TS with `Express` with help of `reflect-metadata`.

## Implemented decorators for contoroller classes

-   `@controller('/root_path_for_controller')`
    This is the main decorator that collects all other decorators via reflect metadata and applies them to methods
-   `@get('path')` or `@post('path')` or `@del('path')` or `@put('path')`
    Express Router methods with path
-   `@use(middlewareHandler)`
    To inject all needed middleware hanlers to method. Can use as many @use decorators as you need
-   `@validateBody(...keys: string[])`
    To validate that Request should contain all passed keys in a body

## Example

```typescript
@controller('/posts')
class PostsController {
    @get('/')
    @use(requireAuth)
    @use(requireAdmin)
    public posts(req: Request, res: Response): void {
        // implementations goes here
    }

    @post('/')
    @validateBody('title', 'description')
    public newPost(req: Request, res: Response): void {
        // implementations goes here
    }
}
```
